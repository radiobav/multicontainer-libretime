# RadioBAV News Scripts

These script are responsibel for playing the news at requested times.
You need to create a folder audio at the same level as theses files are located.

** This implementation is a mess! If you got any ideas how to improve the situation please leave us a message.**

## news.liq
This script is waiting for a connection from the "Libretime".
On connection it will connect back to the master source and will start playing the "news audiofile".
After playing it disconnects and the "libretime" will take care to fade back to the normal programm.

## refresh_news.sh
This script fetches the news from an Onlineservice calles jingel-service.de .
You have to call it peridocaly e.g. a cronjob so that the news get updatet.
It is fetching a news file from the service and saves it as "news.mp3" in the "audio" folder.

## Sample cron entry
You should change the user root to your local user witch has sufficent permissions to write in "/opt/libretime/radiobav-news/audio"
```bash

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
  40 *  *  *  * root /usr/bin/bash /opt/libretime/radiobav-news/referesh_news.sh

```